from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask import request
from flask import jsonify

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////home/tani/flask_application/flask_app/test.db'
SQLALCHEMY_TRACK_MODIFICATIONS = False
db = SQLAlchemy(app)

class Student(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=False, nullable=False)
    roll_number = db.Column(db.Integer, unique=True, nullable=False)
    email = db.Column(db.String(100), unique=True, nullable=False)
    def __init__(self, name, roll_number, email):
        self.name = name
        self.roll_number = roll_number
        self.email = email

    def __repr__(self):
        return '<Student %r>' % self.name

class Course(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True, nullable=False)
    code = db.Column(db.Integer, unique=True, nullable=False)
    def __init__(self, name, code):
        self.name = name
        self.code = code

    def __repr__(self):
        return '<Course %r>' % self.name

@app.route('/')
def hello_world():
    return "hello world"

@app.route('/students/create', methods=["POST"])
def create_student():
    name = request.form['name']
    email = request.form['email']
    roll_number = request.form['roll_number']
    db.create_all()
    new_stud = Student(name, roll_number, email)
    db.session.add(new_stud)
    db.session.commit()
    temp = {}
    temp['status'] = (type(new_stud)==Student)
    return jsonify(temp)

@app.route('/students/', methods=['GET'])
def get_students():
    db.create_all()
    stud_list = Student.query.all()
    strf = ''
    for stud in stud_list:
        strf += stud.name + '\n'
    return strf

@app.route('/students/<int:roll_number>', methods=['POST'])
def edit_rno(roll_number):
    name = request.form['name']
    email = request.form['email']
    roll_number = request.form['roll_number']
    db.create_all()
    stud = Student.query.filter(Student.roll_number == roll_number).delete()
    new_stud = Student(name, roll_number, email)
    db.session.add(new_stud)
    db.session.commit()
    return "edited"

@app.route('/students/delete', methods=['POST'])
def delete_student():
    rno = int(request.form['roll_no_to_delete'])
    print("rno " + str(rno))
    db.create_all()
    stud = Student.query.filter(Student.roll_number == rno).delete()
    db.session.commit()
    return "deleted"

@app.route('/courses/create', methods=["POST"])
def create_course():
    name = request.form['name']
    code = request.form['code']
    db.create_all()
    new_course = Course(name, code)
    db.session.add(new_course)
    db.session.commit()
    temp = {}
    temp['status'] = (type(new_stud)== Course)
    return jsonify(temp)

@app.route('/courses/', methods=['GET'])
def get_courses():
    db.create_all()
    stud_list = Course.query.all()
    strf = ''
    for stud in stud_list:
        strf += stud.name + '\n'
    return strf

@app.route('/courses/<int:code>', methods=['POST'])
def edit_code(code):
    name = request.form['name']
    code = request.form['code']
    db.create_all()
    stud = Course.query.filter(Course.code == code).delete()
    new_stud = Course(name, code)
    db.session.add(new_stud)
    db.session.commit()
    return "edited"

@app.route('/courses/delete', methods=['POST'])
def delete_course():
    code = int(request.form['course_code_to_delete'])
    print("rno " + str(code))
    db.create_all()
    stud = Course.query.filter(Course.code == code).delete()
    db.session.commit()
    return "deleted"
