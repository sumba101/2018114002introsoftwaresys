import sys
import string

commandline=sys.argv
fr = open(commandline[1], 'r')
text = fr.read()

text=text.translate(str.maketrans('', '', string.punctuation))
text=text.split()

fr.close()

table=dict()

fw = open(commandline[2], 'w')
for word in text:
	if word in table:
		table[word]+=1
	else:
		table[word]=1		

for word in text:
	if word in table:
		temp=str(word)+":"+str(table[word])+"\n"
		fw.write(temp)
		table.pop(word)

fw.close()
