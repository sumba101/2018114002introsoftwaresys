from pieces import *  #get all the pieces

class Board:
    def __init__(self):
        self.board = []
        first_row=[castle(1),knight(1),bishop(1),queen(1),king(1),bishop(1),knight(1),castle(1)]
        second_row=[castle(2),knight(2),bishop(2),queen(2),king(2),bishop(2),knight(2),castle(2)]

        for i in range(8):
            temp = []
            
            if i==0:
                for obj in first_row:
                    temp.append(obj) #put the first row on each side
            
            elif i==7:
                for obj in second_row:
                    temp.append(obj) #put the first row on each side
            
            else:    
                for j in range(8):
                    if i==1 :
                        temp.append(pawn(1)) #put pawn objects
                    elif i==6:
                        temp.append(pawn(2)) #put pawn objects        
                    else:
                        temp.append('0') #fill vacancies

            self.board.append(temp)
        
    #handles displaying the board and also calls game_state to check if a player has won    
    def disp(self):
        strf = ''
        lineno=8
        for row in self.board:
            for i in range(2):
                if i%2==0:
                    strf += '   '
                else:
                    strf += ' '+str(lineno)+' '
                    lineno=lineno-1
                
                for obj in row:
                    if i%2 == 0: # Print Board Scaffolding
                        strf += '+---'
                    
                    else: # Print objects
                        strf += '|'
                        
                        if obj=='0':
                            strf += '   ' #put space where vacancy
                        else:    
                            strf += obj.disp() #display whatever object is there
                        
                if i%2 == 0:
                    strf += '+'
                else:
                    strf += '|'
                strf += '\n'
        strf+='   '
        for obj in self.board[-1]:
             strf += '+---'

        strf += '+'   
        print("     A   B    C   D   E   F  G    H")

        print(strf)
        return self.game_state()

    #handles converstion of letter to number    
    def convert_letter_to_num(self,posn): #given the letter of position
        letters = 'abcdefgh'
        i = 0
        for letter in letters:
            if posn == letter:
                return i
            i += 1

    #checks if king is present, current version says player has won if he/she kills the opponents king
    def present(self,tofind):
        for row in self.board:
            for obj in row:
                if obj != '0' and tofind == obj.disp():
                    return True
        return False

    #checks if player 1 or 2 has won
    def game_state(self):
        if not self.present('<1>') :
            return 2 #if player 2 is winner        
        
        elif not self.present('<2>') :
            return 1
        
        return 0 #if game is not over

    #checks if move is valid as per the chess piece, currently returns true always due to assignment specification
    def check_move(self,init,final):
        init[0]=self.convert_letter_to_num(init[0])
        final[0]=self.convert_letter_to_num(final[0])

        #returns 1 none the less as it is not needed for assignment
        return 1 #if aok
        return 0 #if not ok
        
    #makes the move from initial state to final state
    def make_move(self,init,final):
        init[0]=self.convert_letter_to_num(init[0])
        final[0]=self.convert_letter_to_num(final[0])
        
        #just replace final position object with initial position object without fucks on what it is 
        
        self.board[int(8)-int(final[1])][int(final[0])]=self.board[int(8)-int(init[1])][int(init[0])]
        self.board[int(8)-int(init[1])][int(init[0])]='0'
        
