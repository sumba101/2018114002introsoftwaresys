class king():
	def __init__(self, arg):
		self.player = arg

	def disp(self):
		return '<'+str(self.player)+'>'

class bishop():
	def __init__(self, arg):
		self.player = arg
		
	def disp(self):
		return ' B'+str(self.player)

class knight():
	def __init__(self, arg):
		self.player = arg

	def disp(self):
		return ' K'+str(self.player)


class queen():
	def __init__(self, arg):
		self.player = arg
	
	def disp(self):
		return ' Q'+str(self.player)


class castle():
	def __init__(self, arg):
		self.player = arg

	def disp(self):
		return ' R'+str(self.player)

class pawn():
	def __init__(self, arg):
		self.player = arg

	def disp(self):
		return ' P'+str(self.player)
