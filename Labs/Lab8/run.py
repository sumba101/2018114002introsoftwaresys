from projit_board import Board #get the main board class for handling the game

end_game=0 #game continues till end_game is not zero

a = Board()
a.disp()

player=1 #set up player

def winner(number):
	print("Congrats, Player {} won\n".format(number))

print("")
print("Welcome to the Game of Chess")
print("warning:- only enter lower case characters\n")
while end_game == 0:

	print("Enter your move player {}".format(player))
	moves=input().split() #split into moves
	
	if player%2==1 : #player one
		if a.check_move(list(moves[0]),list(moves[1])):
			player+=1
			a.make_move(list(moves[0]),list(moves[1]))
				
		else:
			print("incorrect move\n")	
	
	else :#player two
		if a.check_move(list(moves[0]),list(moves[1])):
			++player
			a.make_move(list(moves[0]),list(moves[1]))

		else:
			print("incorrect move\n")	
		player=1 #cos next is player one		
			
	
	end_game=a.disp()#end game takes value of the winning player
	
winner(end_game)

print("thank you for playing\n")
		