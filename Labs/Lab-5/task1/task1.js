function bubble_Sort(a)
{
    var swapp;
    var n = a.length-1;
    var x = new Array(a.length);
    for(var i = 0 ; i < a.length ; i++) {
    	x[i] = a[i];
    }

    do {
        swapp = false;
        for (var i=0; i < n; i++)
        {
            if (x[i] < x[i+1])
            {
               var temp = x[i];
               x[i] = x[i+1];
               x[i+1] = temp;
               swapp = true;
            }
        }
        n--;
    } while (swapp);
 return x; 
}

function myarray(array){
	sum=0;

	for (var i =array.length - 1; i >= 0; i--) {
		sum=sum + array[i]
	}
	
	product=1;
	for (var i = array.length - 1; i >= 0; i--) {
		product=product * array[i]
	}
	sorted=bubble_Sort(array);
	this.sum= sum
	
	this.product= product;

	this.sort=sorted;

	this.modify=(i,val)=>{
		array[i]=val
	}
	this.display=()=>{
		console.log(array);
	}

}
var a= new myarray([5,2,3,6,1,9,8,7]);
console.log(a.sum)
console.log(a.product)
console.log(a.sort)
console.log(a.modify(1,0))
a.display()
